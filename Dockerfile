# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost1.55-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/unobtanium-official/Unobtanium.git /opt/unobtanium
RUN cd /opt/unobtanium/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/unobtanium/ && \
    make

# ---- Release ---- #
FROM ubuntu:14.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost1.55-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r unobtanium && useradd -r -m -g unobtanium unobtanium
RUN mkdir /data
RUN chown unobtanium:unobtanium /data
COPY --from=build /opt/unobtanium/src/unobtaniumd /usr/local/bin/unobtaniumd
COPY --from=build /opt/unobtanium/src/unobtanium-cli /usr/local/bin/unobtanium-cli
USER unobtanium
VOLUME /data
EXPOSE 65535 65534
CMD ["/usr/local/bin/unobtaniumd", "-datadir=/data", "-conf=/data/unobtanium.conf", "-server", "-reindex", "-txindex", "-printtoconsole"]