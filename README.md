This Dockerfile generates the latest Unobtanium client used in server applications. It is anticipated that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name unobtanium -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/uno-unobtanium:latest
```